const tabs = document.getElementsByClassName('tab');
const sections = document.getElementsByClassName('tabContent');

[...tabs].forEach(tab => tab.addEventListener('click', tabClick));

function tabClick(event) {
    const tabId = event.target.dataset.id;

    [...tabs].forEach((tab, i) => {
        tab.classList.remove('active');
        sections[i].classList.remove('active');
    })

    tabs[tabId - 1].classList.add('active');
    sections[tabId - 1].classList.add('active');
}

$('#tabs-work li').click(function() {
    if($(this).attr('rel')) {
        var category = $(this).attr('rel');
        $('#gall img').fadeOut(300, function() {
            $(this).filter('[class="' + category  + '"]').delay(300).fadeIn(300);
        });
    } else {
        $('#gall img').fadeIn(300);
    }

    return false;
});

$('#gall img').click(function() {
    if($(this).attr('rel')) {
        $('#gall img').hide().filter('[class="' + $(this).attr('rel') + '"]').show();
    } else {
        $('#gall img').show();
    }
    return false;

});


$(document).ready(function() {
    size_li = $("#gall img").size();
    x = 12;
    $('#gall img:lt(' + x + ')').show();
    $('#loadMore').click(function() {
        x = (x + 12 <= size_li) ? x + 12 : size_li;
        $('#gall img:lt(' + x + ')').show();
        $(this).toggle(x < size_li);
    });

});

var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
    showSlides(slideIndex += n);
}

function currentSlide(n) {
    showSlides(slideIndex = n);
}

function showSlides(n) {
    var i;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");

    if (n > slides.length) {
        slideIndex = 1
    }
    if (n < 1){
        slideIndex=slides.length
    }
    for (i=0; i < slides.length; i++){
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++){
        dots[i].className = dots[i].className.replace("active","");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className+= " active";
}